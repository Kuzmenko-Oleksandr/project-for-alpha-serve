AJS.$(document).ready(function () {
    new AJS.RestfulTable({
        autoFocus: false,
        el: AJS.$("#admin-event-rt"),
        allowReorder: false,
        allowEdit: false,
        allowCreate: false,
        allowDelete: false,
        resources: {
            all: AJS.contextPath() + "/rest/restful-table/1.0/admin-restful-table/all",
        },
        columns: [
            {
                id: "email",
                header: "Email"
            }
        ]
    });
});
