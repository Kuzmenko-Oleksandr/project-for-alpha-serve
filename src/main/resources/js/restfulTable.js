AJS.$(document).ready(function () {
    var isChecked = false;
    var form = document.getElementById('toggle');
    form.addEventListener('change', function (e) {
        var toggle = document.getElementById('toggle');
        if (document.querySelectorAll("#event-rt thead")[0]) {
            document.querySelectorAll("#event-rt .aui-restfultable-create")[0].remove()
            document.querySelectorAll("#event-rt thead")[0].remove();
            document.querySelectorAll("#event-rt .ui-sortable")[0].remove();
        }
        isChecked = toggle.checked
        new AJS.RestfulTable({
            autoFocus: false,
            el: AJS.$("#event-rt"),
            allowReorder: true,
            resources: {
                all: AJS.contextPath() + "/rest/restful-table/1.0/restful-table/all?filtered=" + isChecked,
                self: AJS.contextPath() + "/rest/restful-table/1.0/restful-table/self"
            },
            columns: [
                {
                    id: "name",
                    header: "name"
                },
                {
                    id: "surname",
                    header: "surname"
                },
                {
                    id: "position",
                    header: "position"
                },
                {
                    id: "email",
                    header: "email"
                },
                {
                    id: "phone",
                    header: "phone"
                },
                {
                    id: "recruitment",
                    header: "recruitment"
                }
            ]
        });
    });
});