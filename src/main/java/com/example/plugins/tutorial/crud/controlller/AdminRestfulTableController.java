package com.example.plugins.tutorial.crud.controlller;

import com.example.plugins.tutorial.crud.service.RestfulTableService;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/admin-restful-table/")
@Named
public class AdminRestfulTableController {

  private final RestfulTableService restfulTableService;

  @Inject
  public AdminRestfulTableController(RestfulTableService restfulTableService) {
    this.restfulTableService = restfulTableService;
  }

  @GET
  @Path("/all")
  public Response getAllEvents() {
    return Response.ok(new Gson().toJson(restfulTableService.getAllCorrectors())).build();
  }
}
