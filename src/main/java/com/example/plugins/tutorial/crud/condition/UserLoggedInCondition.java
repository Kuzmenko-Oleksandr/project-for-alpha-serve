package com.example.plugins.tutorial.crud.condition;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import javax.inject.Named;

@Named
public class UserLoggedInCondition extends AbstractWebCondition {
  public UserLoggedInCondition() {
  }

  @Override
  public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
    return user != null;
  }
}
