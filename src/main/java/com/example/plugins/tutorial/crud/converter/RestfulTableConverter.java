package com.example.plugins.tutorial.crud.converter;

import com.atlassian.sal.api.user.UserProfile;
import com.example.plugins.tutorial.crud.ao.Employee;
import com.example.plugins.tutorial.crud.dto.EmployeeDTO;
import org.jetbrains.annotations.NotNull;

import javax.inject.Named;

@Named
public class RestfulTableConverter {
  public EmployeeDTO converterToDTO(Employee employee) {
    return EmployeeDTO.builder()
        .id(employee.getID())
        .creatorKey(employee.getCreatorKey())
        .name(employee.getName())
        .surname(employee.getSurname())
        .position(employee.getPosition())
        .email(employee.getEmail())
        .phone(employee.getPhone())
        .recruitment(employee.getRecruitment())
        .build();
  }

  public EmployeeDTO convertToDTO(@NotNull UserProfile profile) {
    return EmployeeDTO.builder()
        .email(profile.getEmail())
        .build();
  }
}
