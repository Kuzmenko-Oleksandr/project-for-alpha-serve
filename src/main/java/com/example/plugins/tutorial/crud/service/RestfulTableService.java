package com.example.plugins.tutorial.crud.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.example.plugins.tutorial.crud.ao.Employee;
import com.example.plugins.tutorial.crud.converter.RestfulTableConverter;
import com.example.plugins.tutorial.crud.dto.EmployeeDTO;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Named
public class RestfulTableService {

  @JiraImport
  private final ActiveObjects activeObjects;
  private final RestfulTableConverter converter;
  @JiraImport("com.atlassian.sal.api.user.UserManager")
  private final UserManager userManager;


  @Inject
  public RestfulTableService(ActiveObjects activeObjects, RestfulTableConverter converter, UserManager userManager) {
    this.activeObjects = activeObjects;
    this.converter = converter;
    this.userManager = userManager;
  }

  public Employee[] getAllFromEmployees() {
    return activeObjects.find(Employee.class);
  }

  public List<EmployeeDTO> getAll(boolean filtered) {
    UserProfile currentUserKey = userManager.getRemoteUser();
    if (currentUserKey != null) {
      String userKey = currentUserKey.getUserKey().getStringValue();
      if (filtered) {
        return Stream.of(getAllFromEmployees())
            .filter(employee -> employee.getCreatorKey().equals(userKey))
            .map(this::constructEmployeeDTO)
            .collect(Collectors.toList());
      } else {
        return Stream.of(getAllFromEmployees())
            .map(this::constructEmployeeDTO)
            .collect(Collectors.toList());
      }
    } else {
      return new ArrayList<>();
    }
  }
  public List<EmployeeDTO> getAllCorrectors() {
    UserProfile currentUserKey = userManager.getRemoteUser();
    if (currentUserKey != null) {
      return Stream.of(getAllFromEmployees())
          .map(Employee::getCreatorKey)
          .map(this::getJiraUser)
          .map(converter::convertToDTO)
          .collect(Collectors.toList());
    } else {
      return new ArrayList<>();
    }
  }

  public UserProfile getJiraUser(String userKey) {
    return userManager.getUserProfile(new UserKey(userKey));
  }

  public Employee getEmployeeById(int id) {
    return activeObjects.get(Employee.class, id);
  }

  public EmployeeDTO constructEmployeeDTO(Employee employee) {
    return converter.converterToDTO(employee);
  }

  public Employee update(Employee employee, EmployeeDTO employeeDTO) {
    if (employeeDTO.getName() != null) {
      employee.setName(employeeDTO.getName());
    }
    if (employeeDTO.getSurname() != null) {
      employee.setSurname(employeeDTO.getSurname());
    }
    if (employeeDTO.getPosition() != null) {
      employee.setPosition(employeeDTO.getPosition());
    }
    if (employeeDTO.getEmail() != null) {
      employee.setEmail(employeeDTO.getEmail());
    }
    if (employeeDTO.getPhone() != null) {
      employee.setPhone(employeeDTO.getPhone());
    }
    if (employeeDTO.getRecruitment() != null) {
      employee.setRecruitment(employeeDTO.getRecruitment());
    }
    UserProfile currentUser = userManager.getRemoteUser();
    if (currentUser != null) {
      employee.setCreatorKey(currentUser.getUserKey().getStringValue());
    }
    employee.save();
    return employee;
  }

  public EmployeeDTO updateEmployee(int id, EmployeeDTO employeeDTO) {
    Employee employee = activeObjects.get(Employee.class, id);
    return converter.converterToDTO(update(employee, employeeDTO));
  }


  public EmployeeDTO createEmployee(EmployeeDTO employeesDTO) {
    Employee employee = activeObjects.create(Employee.class);
    update(employee, employeesDTO);
    return converter.converterToDTO(employee);
  }

  public EmployeeDTO deleteEmployee(int id) {
    Employee employee = this.activeObjects.get(Employee.class, id);
    if (employee != null) {
      this.activeObjects.delete(employee);
    }
    assert employee != null;
    return converter.converterToDTO(employee);
  }
}

