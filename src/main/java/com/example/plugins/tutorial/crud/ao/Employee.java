package com.example.plugins.tutorial.crud.ao;

import net.java.ao.Entity;

public interface Employee extends Entity {

  String getCreatorKey();

  void setCreatorKey(String creatorKey);

  String getName();

  void setName(String name);

  String getSurname();

  void setSurname(String surname);

  String getPosition();

  void setPosition(String position);

  String getEmail();

  void setEmail(String email);

  Integer getPhone();

  void setPhone(Integer phone);

  Long getRecruitment();

  void setRecruitment(Long recruitment);

}
