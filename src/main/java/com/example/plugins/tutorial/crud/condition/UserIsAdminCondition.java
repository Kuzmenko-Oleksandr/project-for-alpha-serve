package com.example.plugins.tutorial.crud.condition;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UserIsAdminCondition extends AbstractWebCondition {

  @JiraImport("com.atlassian.sal.api.user.UserManager")
  private final UserManager userManager;

  @Inject
  public UserIsAdminCondition(UserManager userManager) {
    this.userManager = userManager;
  }

  @Override
  public boolean shouldDisplay(ApplicationUser user, JiraHelper helper) {
    return userManager.isAdmin(new UserKey(user.getKey()));
  }
}
