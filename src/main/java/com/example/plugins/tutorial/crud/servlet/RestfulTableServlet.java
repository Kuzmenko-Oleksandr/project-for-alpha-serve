package com.example.plugins.tutorial.crud.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.example.plugins.tutorial.crud.ao.Employee;
import com.google.common.collect.Maps;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Named
public class RestfulTableServlet extends HttpServlet {
  @JiraImport
  private final ActiveObjects activeObjects;
  @JiraImport
  private final TemplateRenderer templateRenderer;

  @Inject
  public RestfulTableServlet(ActiveObjects activeObjects, TemplateRenderer templateRenderer) {
    this.activeObjects = activeObjects;
    this.templateRenderer = templateRenderer;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    JiraAuthenticationContext jiraAuthenticationContext = ComponentAccessor.getJiraAuthenticationContext();
    ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
    if (loggedInUser != null) {
      Employee[] employees = this.activeObjects.find(
          Employee.class,
          Query.select()
      );
      Map<String, Object> context = Maps.newHashMap();
      context.put("employees", employees);
      templateRenderer.render("/templates/restfulTable.vm", context, response.getWriter());
    } else {
      String url = request.getContextPath() + "/login.jsp?permissionViolation=true&amp;os_destination=" + request.getRequestURI();
      StringBuilder stringBuilder = new StringBuilder();
      response.sendRedirect((stringBuilder.append(url).append("?").append(request.getQueryString())).toString());
    }
  }
}
