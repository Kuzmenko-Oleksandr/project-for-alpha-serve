package com.example.plugins.tutorial.crud.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeDTO {
  private String creatorKey;
  private int id;
  private String name;
  private String surname;
  private String position;
  private String email;
  private Integer phone;
  private Long recruitment;
}
