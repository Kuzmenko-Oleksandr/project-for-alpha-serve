package com.example.plugins.tutorial.crud.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.example.plugins.tutorial.crud.ao.Employee;
import com.google.common.collect.Maps;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Named
public class AdminRestfulTableServlet extends HttpServlet {
  @JiraImport
  private final ActiveObjects activeObjects;
  @JiraImport
  private final TemplateRenderer templateRenderer;
  @JiraImport("com.atlassian.sal.api.user.UserManager")
  private final UserManager userManager;
  @JiraImport
  private final JiraAuthenticationContext authenticationContext;

  @Inject
  public AdminRestfulTableServlet(ActiveObjects activeObjects,
                                  TemplateRenderer templateRenderer,
                                  UserManager userManager,
                                  JiraAuthenticationContext authenticationContext) {
    this.activeObjects = activeObjects;
    this.templateRenderer = templateRenderer;
    this.userManager = userManager;
    this.authenticationContext = authenticationContext;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    ApplicationUser user = authenticationContext.getLoggedInUser();
    String username = user.getUsername();
    if (userManager.isAdmin(new UserKey(user.getKey()))) {
      Employee[] employees = this.activeObjects.find(
          Employee.class,
          Query.select()
      );
      Map<String, Object> context = Maps.newHashMap();
      context.put("employees", employees);
      templateRenderer.render("/templates/aRestfulTable.vm", context, response.getWriter());
    } else {
      String url = request.getContextPath() + "/login.jsp?permissionViolation=true&amp;os_destination=" + request.getRequestURI();
      StringBuilder stringBuilder = new StringBuilder();
      response.sendRedirect((stringBuilder.append(url).append("?").append(request.getQueryString())).toString());
    }
  }
}
