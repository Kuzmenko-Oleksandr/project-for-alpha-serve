package com.example.plugins.tutorial.crud.controlller;

import com.example.plugins.tutorial.crud.dto.EmployeeDTO;
import com.example.plugins.tutorial.crud.service.RestfulTableService;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/restful-table/")
@Named
public class RestfulTableController {

  private final RestfulTableService restfulTableService;

  @Inject
  public RestfulTableController(RestfulTableService restfulTableService) {
    this.restfulTableService = restfulTableService;
  }

  @GET
  @Path("/all")
  public Response getAllEvents(@QueryParam("filtered") boolean filtered) {
    return Response.ok(new Gson().toJson(restfulTableService.getAll(filtered))).build();
  }

  @GET
  @Path("/self/{id}")
  public Response getEvent(@PathParam("id") int id) {
    return Response.ok(restfulTableService.getEmployeeById(id)).build();
  }

  @PUT
  @Path("/self/{id}")
  public Response updateEvent(@PathParam("id") int id, EmployeeDTO employeeDTO) {
    return Response.ok(new Gson().toJson(restfulTableService.updateEmployee(id, employeeDTO))).build();
  }

  @POST
  @Path("/self")
  public Response createEvent(EmployeeDTO employeeDTO) {
    return Response.ok(new Gson().toJson(restfulTableService.createEmployee(employeeDTO))).build();
  }

  @DELETE
  @Path("/self/{id}")
  public Response deleteEvent(@PathParam("id") int id) {
    return Response.ok(new Gson().toJson(restfulTableService.deleteEmployee(id))).build();
  }
}
